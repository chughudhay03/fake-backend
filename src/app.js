// let sample = () => console.log("Hello World");

// sample();

import http from './libs/slhttp';
import ui from './Classes/UI';

const post_title_element = document.querySelector('#post_title');
const post_body_element = document.querySelector('#post_body');
const post_author_element = document.querySelector('#post_author');

document.addEventListener('DOMContentLoaded', fetchPosts);
document.querySelector('#btn_add_post').addEventListener('click', addPosts);

function fetchPosts(e) {
    http.get('http://localhost:3000/posts')
    .then(data => ui.showPosts(data))
    .catch(err => console.warn(err));
}

function addPosts(e) {
    let title = post_title_element.value;
    let body = post_body_element.value;
    let author = post_author_element.value;

    const data = {
        title,
        author,
        body
    };
    console.log(data);
    http.post('http://localhost:3000/posts', data)
    .then(data => {
        ui.createAlert("Post Added Sccessfull", 'alert-success');
        console.log(data);
        ui.clearAllFields();
        fetchPosts();
    })
    .catch(err => console.warn(err));
}
