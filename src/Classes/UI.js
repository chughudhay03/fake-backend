class UI {
    constructor() {
        //container with from and postwrapper
        this.addPostFromCard = document.querySelector('.posts-container .card');
        //container which holds all the cards of the Posts
        this.postsWrapper = document.getElementById('posts');
        //
        this.firstFormGroup = document.querySelector('.posts-container .form-group');
        this.add_post_title_element = document.querySelector('#post_title');
        this.add_post_body_element = document.querySelector('#post_body');
        this.add_post_author_element = document.querySelector('#post_author');
    }
    
    showPosts(posts) {
        let output = '';
        posts.forEach(post => {
            output += `<div class="card mb-2">
            <div class="card-body">
              <h5 class="card-title">${post.title}</h5>
              <p class="card-text">${post.body}.</p>
              <a href="#" class="btn btn-primary pull-right ml-2">Edit</a>
              <a href="#" class="btn btn-danger pull-right">Delete</a>
            </div>
          </div>`;
        });
        this.postsWrapper.innerHTML = output;
    }

    createAlert(message, classes) {
        const divElement = document.createElement('div');
        const classArray = classes.split(" ");

        divElement.className = 'alert';
        classArray.forEach(className => divElement.classList.add(className));
        console.log(this.addPostFromCard);
        console.log(this.firstFormGroup);
        divElement.innerHTML = message;
        this.addPostFromCard.insertBefore(divElement, this.firstFormGroup);
        setTimeout(() => {
            this.clearAlert();
        }, 3000);
    }

    clearAlert() {
        const alertElement = document.querySelector('.alert');
        if(alertElement) {
            alertElement.remove();
        }
    }

    clearAllFields() {
        this.add_post_title_element.value = '';
        this.add_post_body_element.value = '';
        this.add_post_author_element.value = '';
    }
}

const ui = new UI();
export default ui;